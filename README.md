# R Data Types

The goal of today's exercise is to read in data frames from two different sources, identify the different data ypes, and format the columns accordingly and try to show your work and be as thorough as you can in the output file. All datasets used in this exercise were downloaded from Kaggle.com and is saved under data/
- Covid vaccination tables
- Covid variant table

Though an RScript is fine for analysis, scripting, and personal use, RMarkdown format is preferred for sharing process documentation. RMarkdown allows you to run chunks of R code and write paragraphs without having to annotate every single line (unlike an R script). Try to complete the exercise in an RMarkdown file and export it as a Word document.

## Extra Activities
If you were able to complete the activity before time, feel free to either help those around you or try to import and understand other, less commonly used data types like lists and sequences.